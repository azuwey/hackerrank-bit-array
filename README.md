# Bit Array

## Problem

You are given four integers: `N`, `S`, `P`, `Q`. You will use them in order to create the sequence with the following pseudo-code.

```s
a[0] = S (modulo 2^31)
for i = 1 to N-1
  a[i] = a[i-1]*P+Q (modulo 2^31)
```

Your task is to calculate the number of distinct integers in the sequence `a`

### Input Format

Four space separated integers on a single line, `N`, `S`, `P`, and `Q` respectively.

### Constraints

- 1 <= N <= 10^8
- 0 <= S, P, Q <= 2^31

### Output Format

A single integer that denotes the number of distinct integers in the sequence `a`

#### Sample Input

```s
3 1 1 1
```

#### Sample Output

```s
3
```

#### Explanation

`a` = [1, 2, 3]
Hence, there are different integers in the sequence.

## Source

[HackerRank - Bit Array](https://www.hackerrank.com/challenges/bitset-1/problem)